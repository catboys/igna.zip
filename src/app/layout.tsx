import React from "react";
import { Metadata } from "next";
import { Inconsolata } from "next/font/google";

import "@/styles/globals.css";

const inconsolataFont = Inconsolata({ subsets: ["latin"] });

export const metadata: Metadata = {
  metadataBase: new URL("https://www.igna.zip"),
  title: "igna.zip",
  description: "vs. the world",
  themeColor: "#2f3136",
  openGraph: {
    description: "vs. the world",
  },
  robots: {
    index: true,
    follow: true,
    googleBot: {
      index: true,
      follow: true,
      "max-video-preview": -1,
      "max-image-preview": "large",
      "max-snippet": -1,
    },
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inconsolataFont.className}>{children}</body>
    </html>
  );
}
