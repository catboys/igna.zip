import React from "react";
import {
  SiGitlab,
  SiSteam,
  SiLastdotfm,
  SiDiscord,
  SiTelegram,
} from "react-icons/si";
import Status from "@/components/Status";

const Home = () => {
  const socialIcons = [
    { name: "Discord", icon: SiDiscord, url: "/discord" },
    { name: "GitLab", icon: SiGitlab, url: "/gitlab" },
    { name: "Last.fm", icon: SiLastdotfm, url: "/lastfm" },
    { name: "Steam", icon: SiSteam, url: "/steam" },
    { name: "Telegram", icon: SiTelegram, url: "/telegram" },
  ];

  const renderSocialIcons = () => {
    return socialIcons.map((socialIcon) => (
      <a href={socialIcon.url} key={socialIcon.name}>
        <socialIcon.icon size={35} className="icons" name={socialIcon.name} />
      </a>
    ));
  };

  return (
    <div>
      <h1 className="title">igna</h1>
      <h3>
        <Status />
      </h3>
      <h4>{renderSocialIcons()}</h4>
    </div>
  );
};

export default Home;
