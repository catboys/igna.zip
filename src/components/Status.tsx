"use client";

import React, { useEffect, useState } from "react";
import { SiSpotify, SiVisualstudiocode } from "react-icons/si";
import { RiGamepadFill } from "react-icons/ri";
import { useLanyardWS } from "use-lanyard";

const DISCORD_ID = "1166727338986328126";

const Status = (): JSX.Element => {
  const data = useLanyardWS(DISCORD_ID, {
    api: {
      hostname: "l.femboy.band",
      secure: true,
    },
  });
  const [statusText, setStatusText] = useState("Loading...");
  const [statusIcon, setStatusIcon] = useState<string | null>(null);

  useEffect(() => {
    if (data?.listening_to_spotify) {
      const { song, artist } = data.spotify || {};
      setStatusText(`${song} - ${artist}`);
      setStatusIcon("SiSpotify");
    } else if (data?.activities[0]?.name === "Code") {
      setStatusText(`Coding ${data.activities[0].details}`);
      setStatusIcon("SiVisualStudioCode");
    } else if (data?.activities[0]?.type === 0) {
      setStatusText(`Playing ${data.activities[0].name}`);
      setStatusIcon("SiGooglePlay");
    } else {
      setStatusText("Not listening to anything");
      setStatusIcon(null);
    }
  }, [data]);

  const getIcon = (iconName: string) => {
    switch (iconName) {
      case "SiSpotify":
        return <SiSpotify />;
      case "SiGooglePlay":
        return <RiGamepadFill />;
      case "SiVisualStudioCode":
        return <SiVisualstudiocode />;
      default:
        return null;
    }
  };

  return (
    <div className="status">
      {statusIcon && getIcon(statusIcon)}
      <a id="statusText" href="/discord">
        {statusText}
      </a>
    </div>
  );
};

export default Status;
